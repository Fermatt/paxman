﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathmapTile
{
    bool isOpened = false;
    bool isClosed = false;
    
    public Vector2Int TilePos { get; set; }
    public Vector2Int Pos { get; set; }
    public bool Blocking { get; set; }
    public bool GhostStartArea { get; set; }
    public bool PortalArea { get; set; }

    public float TotalCost { get; set; }

    public List<PathmapTile> NeightbourTiles { get; set; }
    public PathmapTile Parent { get; set; } = null;

    public PathmapTile(Vector2Int pos, bool blocking, bool startArea, bool portalArea)
    {
        this.Pos = pos;
        this.Blocking = blocking;
        this.GhostStartArea = startArea;
        this.PortalArea = portalArea;
        NeightbourTiles = new List<PathmapTile>();
    }

    //Devuelve los valores por default
    public void Reset()
    {
        isOpened = false;
        isClosed = false;
        Parent = null;
    }
    
    //Abre el tile para el PathFinding
    public bool Open(PathmapTile _parent, float cost)
    {
        if (!isOpened)
        {
            if (!isClosed)
            {
                isOpened = true;
                TotalCost = cost;
                if (_parent != null)
                {
                    Parent = _parent;
                }
                return true;
            }
        }
        return false;
    }

    //Detecta si fue abierto
    public bool Opened()
    {
        return isOpened;
    }

    //Cierra el tile si estaba abierto
    public bool Closed()
    {
        if (isOpened)
        {
            if (!isClosed)
            {
                isClosed = true;
                isOpened = false;
                return true;
            }
        }
        return false;
    }
}