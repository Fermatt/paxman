﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Map : MonoBehaviour
{
    static public Map instance;

    public int maxDotCount;

    private GameObject cherry = null;

    private int dotCount;

    public int DotCount
    {
        get
        {
            return dotCount;
        }
        set
        {
            dotCount = value;
            //Si no hay mas puntos termina el nivel
            if (dotCount <= 0)
            {
                StartCoroutine(Game.instance.LevelBeaten());
            }
            //Spawn de la primer fruta
            if (fruitSpawn1 == maxDotCount - dotCount)
            {
                if (cherry == null)
                {
                    cherry = Instantiate(fruitPrefab, fruitSpawnPos.position, fruitSpawnPos.rotation).gameObject;
                }
            }
            //Spawn de la segunda fruta
            if (fruitSpawn2 == maxDotCount - dotCount)
            {
                if (cherry == null)
                {
                    Instantiate(fruitPrefab, fruitSpawnPos.position, fruitSpawnPos.rotation);
                }
            }
        }
    }

    public int tileSize = 22;
    public Vector2Int mapSize;

    private List<List<PathmapTile>> tiles = new List<List<PathmapTile>>();
    
    List<PathmapTile> openedTiles;
    List<PathmapTile> closedTiles;

    PathmapTile portalA;
    PathmapTile portalB;

    [SerializeField] private GameObject smallDotPrefab;
    [SerializeField] private GameObject largeDotPrefab;

    [SerializeField] private Transform fruitPrefab;

    [SerializeField] private float fruitSpawn1;
    [SerializeField] private float fruitSpawn2;

    [SerializeField] private Transform fruitSpawnPos;
    
    public Map Instance
    {
        get
        {
            if(instance == null)
            {
                Debug.LogError("NoMapCreated");
                instance = new Map();
            }
            return instance;
        }
    }

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            openedTiles = new List<PathmapTile>();
            closedTiles = new List<PathmapTile>();
            InitMap();
        }
    }

    public bool InitMap()
    {
        //Carga mapa de txt y lo convierte a un tilemap
        string[] lines = System.IO.File.ReadAllLines(Application.dataPath + "/StreamingAssets/Data/map.txt");
        if (lines == null)
            return false;
        for (int y = 0; y < lines.Length; y++)
        {
            tiles.Add(new List<PathmapTile>());
            char[] line = lines[y].ToCharArray();
            for (int x = 0; x < line.Length; x++)
            {
                Vector2Int pos = new Vector2Int(x - (line.Length / 2), -y + (lines.Length / 2));
                bool blocking = line[x] == 'x';
                bool startArea = line[x] == 's';
                bool portalArea = (line[x] == 'p' || line[x] == 'A' || line[x] == 'B');
                PathmapTile tile = new PathmapTile(pos, blocking, startArea, portalArea);
                tile.TilePos = new Vector2Int(x, y);
                if(line[x] == 'A')
                {
                    portalA = tile;
                }
                else if(line[x] == 'B')
                {
                    portalB = tile;
                }
                tiles[y].Add(tile);
                if (line[x] == '.')
                {
                    SmallDot dot = GameObject.Instantiate(smallDotPrefab).GetComponent<SmallDot>();
                    dot.SetPosition(new Vector2(tile.Pos.x * tileSize + tileSize / 2, tile.Pos.y * tileSize));
                    DotCount++;
                }
                else if (line[x] == 'o')
                {
                    BigDot dot = GameObject.Instantiate(largeDotPrefab).GetComponent<BigDot>();
                    dot.SetPosition(new Vector2(tile.Pos.x * tileSize + tileSize/2, tile.Pos.y * tileSize));
                    DotCount++;
                }
            }
        }
        maxDotCount = DotCount;
        mapSize = new Vector2Int(tiles[0].Count, tiles.Count);
        TileNeightbours();
        return true;
    }
    
    //Calcula los tiles vecinos de cada tile
    private void TileNeightbours()
    {
        for (int y = 0; y < tiles.Count; y++)
        {
            for (int x = 0; x < tiles[0].Count; x++)
            {
                PathmapTile tile = tiles[y][x];
                PathmapTile up = GetTile(x, y - 1);
                if (up != null && !up.Blocking)
                    tile.NeightbourTiles.Add(up);

                PathmapTile down = GetTile(x, y + 1);
                if (down != null && !down.Blocking)
                    tile.NeightbourTiles.Add(down);

                PathmapTile right = GetTile(x + 1, y);
                if (right != null && !right.Blocking)
                    tile.NeightbourTiles.Add(right);

                PathmapTile left = GetTile(x - 1, y);
                if (left != null && !left.Blocking)
                    tile.NeightbourTiles.Add(left);
            }
        }
        portalA.NeightbourTiles.Add(portalB);
        portalB.NeightbourTiles.Add(portalA);
    }

    internal bool TileIsValid(int tileX, int tileY)
    {
        if ((tileX >= 0 && tileX < tiles[0].Count) && (tileY >= 0 && tileY < tiles.Count))
        {
            if (!tiles[tileY][tileX].Blocking)
            {
                return true;
            }
        }
        return false;
    }

    internal bool TileIsStart(int tileX, int tileY)
    {
        if ((tileX >= 0 && tileX < tiles[0].Count) && (tileY >= 0 && tileY < tiles.Count))
        {
            if (tiles[tileY][tileX].GhostStartArea)
            {
                return true;
            }
        }
        return false;
    }

    //Devuelve la posicion del otro portal B cuando se entra por el portal A y viceversa
    internal PathmapTile PortalTeleport(int tileX, int tileY, MobileEntity.MovementDirection direction)
    {
        if (tileX == portalA.TilePos.x && tileY == portalA.TilePos.y && direction == MobileEntity.MovementDirection.Left)
        {
            return portalB;
        }
        else if (tileX == portalB.TilePos.x && tileY == portalB.TilePos.y && direction == MobileEntity.MovementDirection.Right)
        {
            return portalA;
        }
        return null;
    }
    
    bool OpenTile(PathmapTile tile, PathmapTile parent, float cost)
    {
        if (tile.Open(parent, cost))
        {
            openedTiles.Add(tile);
            return true;
        }
        else if (tile.Opened())
        {
            if (cost < tile.TotalCost)
            {
                tile.TotalCost = cost;
                tile.Parent = parent;
            }
        }
        return false;
    }

    bool CloseTile(PathmapTile tile)
    {
        if (tile.Closed())
        {
            openedTiles.Remove(tile);
            closedTiles.Add(tile);
            return true;
        }
        return false;
    }

    private void ResetTiles()
    {
        for (int i = 0; i < closedTiles.Count; i++)
        {
            closedTiles[i].Reset();
        }
        for (int i = 0; i < openedTiles.Count; i++)
        {
            openedTiles[i].Reset();
        }
        openedTiles.Clear();
        closedTiles.Clear();
    }

    private void OpenNextTiles(PathmapTile tile, int offset = 0)
    {
        List<PathmapTile> nextTiles = tile.NeightbourTiles;
        for (int i = 0; i < nextTiles.Count; i++)
        {
            if (!nextTiles[i].Blocking)
            {
                float dis = (tile.Pos - nextTiles[i].Pos).magnitude;
                OpenTile(nextTiles[i], tile, dis + offset);
            }
        }
    }

    //Crea un Path para los fantasmas basandose en la posicion actual.
    public List<PathmapTile> GetPath(int currentTileX, int currentTileY, Ghost.MovementDirection currentDir, int targetX, int targetY, Ghost.MovementDirection targetDir)
    {
        PathmapTile fromTile = GetTile(currentTileX, currentTileY);
        PathmapTile toTile = GetTile(targetX, targetY);
        
        //currentDir = MobileEntity.MovementDirection.None;
        //targetDir = MobileEntity.MovementDirection.None;

        if (fromTile.Blocking || toTile.Blocking)
            return null;

        if (fromTile == toTile)
        {
            List<PathmapTile> path2 = Pathfind(toTile);
            ResetTiles();
            return path2;
        }

        OpenTile(fromTile, null, 0);

        PathmapTile originTile = fromTile;

        int dirrOffset = 10;

        //Calcula el primer tile para continuar la direccion original
        if (currentDir != MobileEntity.MovementDirection.None)
        {
            switch (currentDir)
            {
                case MobileEntity.MovementDirection.Down:
                    if (fromTile.TilePos.y + 1 >= tiles.Count)
                    {
                        break;
                    }
                    originTile = GetTile(fromTile.TilePos.x, fromTile.TilePos.y + 1);
                    if (!originTile.Blocking)
                    {
                        CloseTile(fromTile);
                        OpenTile(originTile, fromTile, 0);
                        OpenNextTiles(fromTile, dirrOffset);
                    }
                    break;
                case MobileEntity.MovementDirection.Left:
                    if (fromTile.TilePos.x - 1 < 0)
                    {
                        break;
                    }
                    originTile = GetTile(fromTile.TilePos.x - 1, fromTile.TilePos.y);
                    if (!originTile.Blocking)
                    {
                        CloseTile(fromTile);
                        OpenTile(originTile, fromTile, 0);
                        OpenNextTiles(fromTile, dirrOffset);
                    }
                    break;
                case MobileEntity.MovementDirection.Right:
                    if (fromTile.TilePos.x + 1 >= tiles[0].Count)
                    {
                        break;
                    }
                    originTile = GetTile(fromTile.TilePos.x + 1, fromTile.TilePos.y);
                    if (!originTile.Blocking)
                    {
                        CloseTile(fromTile);
                        OpenTile(originTile, fromTile, 0);
                        OpenNextTiles(fromTile, dirrOffset);
                    }
                    break;
                case MobileEntity.MovementDirection.Up:
                    if (fromTile.TilePos.y - 1 < 0)
                    {
                        break;
                    }
                    originTile = GetTile(fromTile.TilePos.x, fromTile.TilePos.y - 1);
                    if (!originTile.Blocking)
                    {
                        CloseTile(fromTile);
                        OpenTile(originTile, fromTile, 0);
                        OpenNextTiles(fromTile, dirrOffset);
                    }
                    break;
            }
        }

        //Calcula la direccion por donde encontrar el objetivo
        if (targetDir != MobileEntity.MovementDirection.None)
        {
            PathmapTile destinyTile;
            switch (targetDir)
            {
                case MobileEntity.MovementDirection.Down:
                    if(toTile.TilePos.y + 1 >= tiles.Count)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                        break;
                    }
                    destinyTile = GetTile(toTile.TilePos.x, toTile.TilePos.y + 1);
                    if (destinyTile.Blocking)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                    }
                    break;
                case MobileEntity.MovementDirection.Left:
                    if (toTile.TilePos.x - 1 < 0)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                        break;
                    }
                    destinyTile = GetTile(toTile.TilePos.x - 1, toTile.TilePos.y);
                    if (destinyTile.Blocking)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                    }
                    break;
                case MobileEntity.MovementDirection.Right:
                    if (toTile.TilePos.x + 1 >= tiles[0].Count)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                        break;
                    }
                    destinyTile = GetTile(toTile.TilePos.x + 1, toTile.TilePos.y);
                    if (destinyTile.Blocking)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                    }
                    break;
                case MobileEntity.MovementDirection.Up:
                    if (toTile.TilePos.y - 1 < 0)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                        break;
                    }
                    destinyTile = GetTile(toTile.TilePos.x, toTile.TilePos.y - 1);
                    if (destinyTile.Blocking)
                    {
                        targetDir = MobileEntity.MovementDirection.None;
                    }
                    break;
            }
        }

        List<PathmapTile> path = null;
        while (openedTiles.Count > 0)
        {
            PathmapTile tile = SelectTile(toTile.Pos);
            if (tile == toTile)
            {
                PathmapTile dirTile = null;
                //Detecta si es la direccion deseada. Si no lo es guarda el path por si no hay otro camino y sigue calculando otros caminos.
                switch (targetDir)
                {
                    case MobileEntity.MovementDirection.Down:
                        dirTile = GetTile(toTile.TilePos.x, toTile.TilePos.y + 1);
                        break;
                    case MobileEntity.MovementDirection.Left:
                        dirTile = GetTile(toTile.TilePos.x - 1, toTile.TilePos.y);
                        break;
                    case MobileEntity.MovementDirection.Right:
                        dirTile = GetTile(toTile.TilePos.x + 1, toTile.TilePos.y);
                        break;
                    case MobileEntity.MovementDirection.Up:
                        dirTile = GetTile(toTile.TilePos.x, toTile.TilePos.y - 1);
                        break;
                    case MobileEntity.MovementDirection.None:
                        dirTile = tile.Parent;
                        break;
                }
                if (dirTile == tile.Parent)
                {
                    path = Pathfind(tile);
                    ResetTiles();
                    return path;
                }
                else
                {
                    path = Pathfind(tile);
                    CloseTile(tile);
                    closedTiles.Remove(tile);
                    tile.Reset();
                }
            }
            else
            {
                OpenNextTiles(tile);
                CloseTile(tile);
            }
        }
        ResetTiles();
        return path;
    }

    //Devuelve el camino completo
    private List<PathmapTile> Pathfind(PathmapTile tile)
    {
        List<PathmapTile> path = new List<PathmapTile>();
        PathmapTile Parent = tile;
        while (Parent != null)
        {
            path.Add(Parent);
            Parent = Parent.Parent;
        }
        return path;
    }

    //Elige el proximo tile dependiendo del costo que tiene en llegar a ese punto.
    private PathmapTile SelectTile(Vector2Int des)
    {
        PathmapTile selectedTile = openedTiles[0];
        for (int i = 1; i < openedTiles.Count; i++)
        {
            if ((openedTiles[i].TotalCost + (openedTiles[i].Pos - des).magnitude) < (selectedTile.TotalCost + (selectedTile.Pos - des).magnitude))
            {
                selectedTile = openedTiles[i];
            }
        }
        return selectedTile;
    }

    //Devuelve el tile en el que se encuentra
    public PathmapTile GetTile(int tileX, int tileY)
    {
        if ((tileX >= 0 && tileX < tiles[0].Count) && (tileY >= 0 && tileY < tiles.Count))
            return tiles[tileY][tileX];
        else
            return null;
    }
    
    //Devuelve el tile en base a la posicion en el mundo
    public Vector2Int GetPosInMap(Vector3 pos)
    {
        return new Vector2Int((int)(pos.x / tileSize + tiles[0].Count / 2), (int)(-pos.y / tileSize + tiles.Count / 2));
    }

    //Devuelve el tile libre mas cercano a un punto en el mundo
    public Vector2Int GetClosestToPos(int origX, int origY)
    {
        origX = Mathf.Clamp(origX, 0, tiles[0].Count - 1);
        origY = Mathf.Clamp(origY, 0, tiles.Count - 1);
        int x = origX;
        int y = origY;
        Vector2Int finalPos = new Vector2Int();
        int tileLimit = 1;
        bool yPos = false;
        bool yNeg = false;
        bool xPos = false;
        bool xNeg = false;
        while (GetTile(x, y).Blocking)
        {
            if (!yPos)
            {
                if (y + 1 < tiles.Count)
                {
                    y++;
                    if (y - origY >= tileLimit)
                    {
                        yPos = true;
                    }
                }
                else
                {
                    yPos = true;
                }
            }
            else if (!xPos)
            {
                if (x + 1 < tiles[0].Count)
                {
                    x++;
                    if (x - origX >= tileLimit)
                    {
                        xPos = true;
                    }
                }
                else
                {
                    xPos = true;
                }
            }
            else if (!yNeg)
            {
                if (y - 1 >= 0)
                {
                    y--;
                    if (y - origY <= -tileLimit)
                    {
                        yNeg = true;
                    }
                }
                else
                {
                    yNeg = true;
                }
            }
            else if (!xNeg)
            {
                if (x - 1 >= 0)
                {
                    x--;
                    if (x - origX <= -tileLimit)
                    {
                        xNeg = true;
                    }
                }
                else
                {
                    xNeg = true;
                }
            }
            else
            {
                yPos = false;
                yNeg = false;
                xPos = false;
                xNeg = false;
                tileLimit++;
            }
        }
        finalPos.x = x;
        finalPos.y = y;
        return finalPos;
    }
}

