﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHighscore : MonoBehaviour
{
    [SerializeField] private bool highscore = false;
    private int savedHighScore = 0;

    private Text text;
    private string origText;

    // Carga el highscore de PlayerPrefs. Si no existe, lo deja en 10000. Actualiza el texto.
    void Start()
    {
        text = GetComponent<Text>();
        if (highscore)
        {
            //if(PlayerPrefs.GetInt("highscore"))
            savedHighScore = 10000;
            int score = PlayerPrefs.GetInt("highscore");
            if(score > savedHighScore)
            {
                savedHighScore = score;
            }
            origText = text.text;
            text.text = origText + savedHighScore;
        }
        else
        {
            origText = text.text;
            text.text = origText + Game.instance.Score;
        }
    }

    //Actualiza el texto y guarda el valor.
    public void UpdateUI(float score)
    {
        if (!highscore)
        {
            text.text = origText + score;
        }
        else if (savedHighScore < score)
        {
            savedHighScore = (int)score;
            PlayerPrefs.SetInt("highscore", savedHighScore);
            text.text = origText + score;
        }
    }

    private void OnDestroy()
    {
        if(highscore)
        {
            PlayerPrefs.Save();
        }
    }
}
