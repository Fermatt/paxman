﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartLevel : MonoBehaviour
{
    //Actualiza la escena en base al input.
    void Update()
    {
        if(Input.GetButtonDown("Enter"))
        {
            SceneManager.LoadScene("Level");
        }
        else if(Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
        }
    }
}
