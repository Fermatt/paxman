﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    //Guarda los tiempos de los fantasmas por nivel
    [Serializable]
    public struct LevelTime
    {
        public int toLevel;
        public List<GhostTimeBehaviour> timeBehaviours;
    }

    //Guarda los tiempos de comportamientos para los fantasmas
    [Serializable]
    public class GhostTimeBehaviour
    {
        public float time;
        public Ghost.Behaviour behaviour;
        public bool active;
    }

    static public Game instance;
    static private int levelCounter = 0;
    static private uint score;
    static private uint lives;
    static private bool extraLife;

    private List<Ghost> ghosts;
    private int ghostAmount;
    private float myGhostGhostCounter;
    private float ghostLevelTimer;
    private uint deadGhostCounter = 0;
    private Map map;
    private List<GhostTimeBehaviour> timeBehaviour;

    private bool gameStart = false;

    [SerializeField] private GameObject pacManPrefab;
    [SerializeField] private Transform pacManStartPos;
    [SerializeField] private GameObject ghostPrefab;
    [SerializeField] private Text gameOverText;
    [SerializeField] private Text readyText;
    [SerializeField] private UpdateHighscore[] scoreUpdate;
    [SerializeField] private Ghost.GhostType[] ghostStartPos;
    [SerializeField] private Transform ghostRespawnPos;
    [SerializeField] private GameObject[] livesUI;

    [SerializeField] private float ghostCollisionRadius = 16f;
    [SerializeField] private float blueGhostTime = 10f;
    [SerializeField] private float blueGhostDeadAnimTime = 1f;
    [SerializeField] private uint blueGhostScore = 200;
    [SerializeField] private uint generalMaxSpeed = 5;

    [SerializeField] private List<LevelTime> levelTime;

    [SerializeField] private float startTime1 = 2;
    [SerializeField] private float startTime2 = 2;

    [SerializeField] private float deadTime1 = 1;
    [SerializeField] private float deadTime2 = 5;

    [SerializeField] private float gameOverTime = 5;
    [SerializeField] private int bigDotMaxLevel = 20;

    [SerializeField] private int extraLifeScore = 10000;


    public PacMan Avatar { get; private set; }
    public uint Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            for(int i = 0; i < scoreUpdate.Length; i++)
                scoreUpdate[i].UpdateUI(score);
            //Si pasa el valor especificado, le da una vida extra. Solo ocurre una vez
            if(score >=extraLifeScore && !extraLife)
            {
                UpdateLives(lives+1);
                extraLife = true;
            }
        }
    }

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            //Aplica la cantidad de vidas al comienzo del juego
            if(lives <= 0)
            {
                lives = 3;
            }
        }
    }

    void Start()
    {
        StartCoroutine(OnInit());
    }

    private IEnumerator OnInit()
    {
        levelCounter++;
        ghostLevelTimer = 0;
        map = Map.instance;

        yield return new WaitForSeconds(startTime1);

        // Crea y posiciona a Pacman
        Avatar = GameObject.Instantiate(pacManPrefab).GetComponent<PacMan>();
        Avatar.Spawn(pacManStartPos);
        //Crea y posiciona los fantasmas.
        ghosts = new List<Ghost>();
        ghostAmount = ghostStartPos.Length;
        for (int g = 0; g < ghostAmount; g++)
        {
            ghosts.Add(GameObject.Instantiate(ghostPrefab).GetComponent<Ghost>());
            ghosts[g].Spawn(ghostStartPos[g].StartPos);
            ghosts[g].SetGhostColor(ghostStartPos[g].color, ghostStartPos[g].sprite);
            ghosts[g].SetWanderTiles(ghostStartPos[g].wanderTiles);
            List<Ghost.LeaveCage> lc = ghostStartPos[g].leaveCage;
            for (int i = 0; i < lc.Count; i++)
            {
                if (levelCounter <= lc[i].toLevel || lc[i].toLevel == -1)
                {
                    ghosts[g].SetLeaveCage(lc[i]);
                    i = lc.Count;
                }
            }
            ghosts[g].CheckLeaveCage(0);
        }
        for (int i = 0; i < levelTime.Count; i++)
        {
            if (levelCounter <= levelTime[i].toLevel || levelTime[i].toLevel == -1)
            {
                timeBehaviour = levelTime[i].timeBehaviours;
                i = levelTime.Count;
            }
        }
        
        readyText.enabled = true;

        yield return new WaitForSeconds(startTime2);
        
        readyText.enabled = false;

        Avatar.SetIsMoving(true);
        for (int g = 0; g < ghostAmount; g++)
        {
            ghosts[g].SetIsMoving(true);
        }

        gameStart = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Mata la aplicacion si presiona ESC
        if (!HandleInput())
        {
            ResetValues();
            SceneManager.LoadScene("Menu");
            return;
        }

        //Maneja los tiempos de los fantasmas y los cambia en el momento.
        if (gameStart)
        {
            ghostLevelTimer -= Time.deltaTime;

            if (ghostLevelTimer <= 0)
            {
                for (int i = 0; i < timeBehaviour.Count; i++)
                {
                    if (!timeBehaviour[i].active)
                    {
                        timeBehaviour[i].active = true;
                        for (int j = 0; j < ghosts.Count; j++)
                        {
                            ghosts[j].SetBehaviour(timeBehaviour[i].behaviour);
                        }
                        ghostLevelTimer = timeBehaviour[i].time;
                        i = timeBehaviour.Count;
                    }
                }
            }

            //Calcula tiempo de debilidad del fantasma.
            myGhostGhostCounter -= Time.deltaTime;

            //Detecta si se termino el tiempo del PowerUp
            if (myGhostGhostCounter <= 0)
            {
                for (int g = 0; g < ghosts.Count; g++)
                {
                    ghosts[g].IsClaimable = false;
                }
            }

            //Detecta colision con fantasmas
            for (int g = 0; g < ghosts.Count; g++)
            {
                if ((ghosts[g].GetPosition() - Avatar.GetPosition()).magnitude < ghostCollisionRadius && !Avatar.IsDead)
                {
                    if (!ghosts[g].IsClaimable && !ghosts[g].IsDead)
                    {
                        UpdateLives(lives - 1);

                        for (int h = 0; h < ghosts.Count; h++)
                        {
                            ghosts[h].SetIsMoving(false);
                            ghosts[h].gameObject.SetActive(false);
                        }

                        Avatar.SetIsMoving(false);
                        Avatar.IsDead = true;
                        StartCoroutine(Dead());
                    }
                    else if(!ghosts[g].IsDead)
                    {
                        StartCoroutine(GhostDead(g));
                    }
                }
            }
        }
    }

    //Mata el fantasma que comio pacman, genera una pequeña pausa y suma puntos.
    private IEnumerator GhostDead(int g)
    {
        Avatar.SetIsMoving(false);
        for (int h = 0; h < ghosts.Count; h++)
        {
            ghosts[h].SetIsMoving(false);
        }

        deadGhostCounter++;
        uint newScore = blueGhostScore;
        for(int i = 1; i < deadGhostCounter; i++)
        {
            newScore *= 2;
        }
        UpdateScore(newScore);
        ghosts[g].IsDead = true;
        ghosts[g].Die();
        yield return new WaitForSeconds(blueGhostDeadAnimTime);

        Avatar.SetIsMoving(true);
        for (int h = 0; h < ghosts.Count; h++)
        {
            ghosts[h].SetIsMoving(true);
        }
    }

    //Mata a pacman, muestra la animacion y reinicia las posiciones.
    private IEnumerator Dead()
    {
        Avatar.StopAnim();
        yield return new WaitForSeconds(deadTime1);

        Avatar.PlayDeadAnim();

        yield return new WaitForSeconds(deadTime2);

        if (lives <= 0)
        {
            StartCoroutine(GameOver());
        }
        else
        {
            readyText.enabled = true;

            Avatar.Respawn();
            Avatar.PlayStartAnim();
            Avatar.StopAnim();

            for (int h = 0; h < ghosts.Count; h++)
            {
                ghosts[h].gameObject.SetActive(true);
                ghosts[h].Respawn();
            }

            yield return new WaitForSeconds(startTime2);

            for (int h = 0; h < ghosts.Count; h++)
            {
                ghosts[h].Kill();
            }

            readyText.enabled = false;

            Avatar.SetIsMoving(true);
            Avatar.IsDead = false;
        }
    }
    
    private IEnumerator GameOver()
    {
        gameOverText.enabled = true;
        yield return new WaitForSeconds(deadTime2);
        ResetValues();
        SceneManager.LoadScene("Menu");
    }

    public IEnumerator LevelBeaten()
    {
        for (int h = 0; h < ghosts.Count; h++)
        {
            ghosts[h].gameObject.SetActive(false);
        }
        Avatar.gameObject.SetActive(false);
        yield return new WaitForSeconds(gameOverTime);
        SceneManager.LoadScene("Level");
    }

    //Actualiza las vidas y su UI
    private void UpdateLives(uint v)
    {
        lives = v;
        for(int i = 0; i < livesUI.Length; i++)
        {
            if(i+1 < lives)
            {
                livesUI[i].SetActive(true);
            }
            else
            {
                livesUI[i].SetActive(false);
            }
        }
    }

    public void UpdateScore(uint scoreGain)
    {
        Score += scoreGain;
    }
    
    public bool HandleInput()
    {
        if (Input.GetButton("Cancel"))
        {
            return false;
        }
        return true;
    }

    //Comienza el tiempo de vulnerabilidad de los fantasmas y cambia la velocidad.
    public void BigDotStart()
    {
        myGhostGhostCounter = blueGhostTime - levelCounter/2;
        if(myGhostGhostCounter <= 2)
        {
            myGhostGhostCounter = 2;
        }
        for (int g = 0; g < ghosts.Count; g++)
        {
            if(!ghosts[g].IsDead && levelCounter <= bigDotMaxLevel)
                ghosts[g].IsClaimable = true;
        }
        Avatar.SetFrightTimer(blueGhostTime);
        deadGhostCounter = 0;
    }

    public List<Ghost> GetGhostsList()
    {
        return ghosts;
    }

    public uint GetSpeed()
    {
        return generalMaxSpeed;
    }

    public int GetLevelCounter()
    {
        return levelCounter;
    }

    //Devuelve los valores estaticos a su valor original.
    private void ResetValues()
    {
        instance = null;
        levelCounter = 0;
        score = 0;
        extraLife = false;
        lives = 3;
    }
}