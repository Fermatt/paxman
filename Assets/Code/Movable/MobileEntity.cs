﻿using UnityEngine;

public class MobileEntity : Entity
{
    //Todos los tipos de direccion para objetos moviles.
    public enum MovementDirection : int
    {
        Up = 0,
        Down = 1,
        Left = 2,
        Right = 3,
        DirectionCount = 4,
        None = 5
    };

    protected int currentTileX;
    protected int currentTileY;
    protected int nextTileX;
    protected int nextTileY;

    protected bool isMoving = true;
    
    [SerializeField] protected MovementDirection currentDir;
    [SerializeField] protected Transform respawnLocation;

    // Start is called before the first frame update
    protected void Start()
    {
        base.Start();
        nextTileX = currentTileX;
        nextTileY = currentTileY;
        isMoving = false;
    }

    protected void OnUpdate(float speed)
    {
        if (isMoving)
        {
            //Saca la direccion de movimiento en base al destino.
            Vector2 destination = mapCache.GetTile(nextTileX, nextTileY).Pos * mapCache.tileSize;
            destination.x += mapCache.tileSize / 2;
            if (destination == Vector2.zero)
                return;

            Vector2 direction = new Vector2(destination.x - transform.position.x, destination.y - transform.position.y);

            //Calcula la direccion en base al movimiento.
            if (direction.x > 0.5)
            {
                currentDir = MovementDirection.Right;
            }
            else if (direction.x < -0.5)
            {
                currentDir = MovementDirection.Left;
            }
            else if (direction.y > 0.5)
            {
                currentDir = MovementDirection.Up;
            }
            else if (direction.y < -0.5)
            {
                currentDir = MovementDirection.Down;
            }

            float distanceToMove = Time.deltaTime * speed;

            //Movimiento

            if (distanceToMove > direction.magnitude)
            {
                SetPosition(destination);
                currentTileX = nextTileX;
                currentTileY = nextTileY;
            }
            else
            {
                Vector2 position = new Vector2(transform.position.x, transform.position.y);
                direction.Normalize();
                SetPosition(position + direction * distanceToMove);
            }

            PathmapTile portal = mapCache.PortalTeleport(currentTileX, currentTileY, currentDir);
            if (portal != null)
            {
                SetCurrentTiles(portal.TilePos);
                SetNextTile(portal.TilePos);
                SetPosition(portal.Pos * mapCache.tileSize);
            }
        }
    }

    public bool IsAtDestination()
    {
        if (currentTileX == nextTileX && currentTileY == nextTileY)
        {
            return true;
        }
        return false;
    }

    public void SetNextTile(Vector2Int nextPos)
    {
        nextTileX = nextPos.x;
        nextTileY = nextPos.y;
    }

    public int GetCurrentTileX()
    {
        return currentTileX;
    }

    public int GetCurrentTileY()
    {
        return currentTileY;
    }

    private void SetCurrentTiles(Vector2Int newPos)
    {
        currentTileX = newPos.x;
        currentTileY = newPos.y;
    }
    
    public void SetTilePosition(Transform origPos)
    {
        Vector2Int newPos = mapCache.GetPosInMap(origPos.position);
        SetCurrentTiles(newPos);
        transform.position = origPos.position;
    }
    
    //Devuelve al jugador al punto de inicio.
    public void Respawn()
    {
        Start();
        SetTilePosition(respawnLocation);
        nextTileX = currentTileX;
        nextTileY = currentTileY;
    }

    //Prepara la posicion inicial de spawn
    public void Spawn(Transform spawnPos)
    {
        respawnLocation = spawnPos;
        Respawn();
    }

    //Habilita el movimiento de los objetos moviles
    public void SetIsMoving(bool moving)
    {
        isMoving = moving;
    }
}