﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MobileEntity
{
    //Struct que marca la cantidad minima de puntos o tiempo para liberar el fantasma
    [Serializable]
    public struct LeaveCage
    {
        public int toLevel;
        public int minDots;
        public float minTime;
    }

    //Struct de velocidades por nivel
    [Serializable]
    public struct SpeedInLevel
    {
        public int toLevel;

        [Range(0f, 1f)]
        public float normalSpeed;

        [Range(0f, 1f)]
        public float frightSpeed;

        [Range(0f, 1f)]
        public float tunnelSpeed;

        [Range(0f, 2f)]
        public float deadSpeed;
    }

    //Struct de color, imagen, posicion y path de fantasmas.
    [Serializable]
    public struct GhostType
    {
        public GhostColor color;
        public Sprite sprite;
        public Transform StartPos;
        public List<WanderTiles> wanderTiles;
        public List<LeaveCage> leaveCage;
    }

    //Tiles para el pathfinding
    [Serializable]
    public struct WanderTiles
    {
        public Transform pos;
        public MovementDirection dir;
    }

    //Comportamientos de fantasma
    public enum Behaviour
    {
        Wander,
        Chase,
        Fear,
    }

    //Colores de los fantasmas
    public enum GhostColor
    {
        Cyan,
        Red,
        Pink,
        Orange,
    }

    private GhostColor ghostColor;

    private int desiredMovementX;
    private int desiredMovementY;

    private Behaviour myBehaviour;
    private List<PathmapTile> path = new List<PathmapTile>();

    private List<WanderTiles> wanderTiles;
    private LeaveCage leaveCage;
    private int wanderIndex;

    private int movementCounter = 0;
    private SpeedInLevel currentSpeed;
    private Sprite colorSprite;

    private bool startRoutine = false;
    private float startTimer = 999;

    private bool justKilled = false;

    [SerializeField] private float normalSpeed = 30f;
    [SerializeField] private float deadSpeed = 120f;

    [SerializeField] private float updateDistance = 5f;

    [SerializeField] private int orangeRandomRadius = 5;
    [SerializeField] private int cyanRandomRadius = 2;

    [SerializeField] private int maxMovement = 6;

    [SerializeField] private Vector2Int basePos = new Vector2Int(13,14);

    [SerializeField] private List<SpeedInLevel> speedInLevel;
    
    [SerializeField] private Sprite vulnerableSprite;
    [SerializeField] private Sprite deadSprite;

    [SerializeField] private float respawnTime = 5f;


    private bool isClaimable;
    public bool IsClaimable
    {
        get
        {
            return isClaimable;
        }
        set
        {
            isClaimable = value;
            SetSprite(vulnerableSprite);
            //Cambia de direccion en el momento que se vuelve vulnerable
            if (!IsDead && IsClaimable)
            {
                if (currentDir == MovementDirection.Down)
                {
                    currentDir = MovementDirection.Up;
                }
                else if (currentDir == MovementDirection.Up)
                {
                    currentDir = MovementDirection.Down;
                }
                else if (currentDir == MovementDirection.Left)
                {
                    currentDir = MovementDirection.Right;
                }
                else if (currentDir == MovementDirection.Right)
                {
                    currentDir = MovementDirection.Left;
                }
                BehaveVulnerable();
            }
            //Cambia el sprite a fantasma muerto
            else if(!IsDead)
            {
                SetSprite(colorSprite);
            }
        }
    }
    private bool isDead;
    public bool IsDead
    {
        get
        {
            return isDead;
        }
        set
        {
            isDead = value;
            if(IsDead)
            {
                Die();
            }
            //Vuelve a fantasma normal
            else if(!isClaimable)
            {
                SetSprite(colorSprite);
            }
        }
    }

    public void SetBehaviour(Behaviour b)
    {
        myBehaviour = b;
    }

    private void Awake()
    {
        currentDir = MovementDirection.None;
    }

    public void SetGhostColor(GhostColor newColor, Sprite sprite)
    {
        ghostColor = newColor;
        colorSprite = sprite;
        SetSprite(colorSprite);
    }

    private void SetSprite(Sprite sprite)
    {
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

    public void SetWanderTiles(List<WanderTiles> wanderPos)
    {
        wanderTiles = wanderPos;
    }

    public void SetLeaveCage(LeaveCage _leaveCage)
    {
        leaveCage = _leaveCage;
        startTimer = leaveCage.minTime;
    }

    void Start()
    {
        base.Start();

        IsClaimable = false;
        IsDead = false;
        
        desiredMovementX = 0;
        desiredMovementY = -1;
        //Revisa en que nivel esta el juego y carga la velocidad del respectivo nivel
        for (int i = 0; i < speedInLevel.Count; i++)
        {
            if (gameCache.GetLevelCounter() <= speedInLevel[i].toLevel || speedInLevel[i].toLevel == -1)
            {
                currentSpeed = speedInLevel[i];
                i = speedInLevel.Count;
            }
        }
    }

    public void Update()
    {
        if (startRoutine)
        {
            //Detecta si llego a destino y actua en base a su estado.
            if (IsAtDestination())
            {
                movementCounter++;
                //Si faltan tiles y no se cancelo el path, sigue con el siguiente tile en el path.
                if (path.Count > 0 && (movementCounter < maxMovement || myBehaviour == Behaviour.Wander || IsDead))
                {
                    PathmapTile nextTile = path[path.Count - 1];
                    SetNextTile(nextTile.TilePos);
                    path.RemoveAt(path.Count - 1);
                }
                //Sino, calcula un nuevo path.
                else
                {
                    movementCounter = 0;
                    if (IsClaimable && !IsDead)
                    {
                        BehaveVulnerable();
                    }
                    else
                    {
                        switch (myBehaviour)
                        {
                            case Behaviour.Chase:
                                BehaveChase();
                                break;
                            case Behaviour.Wander:
                                BehaveWander();
                                break;
                            default:
                                BehaveWander();
                                break;
                        }
                    }
                    if (path == null)
                    {
                        Debug.LogError("Path null");
                    }
                    //Se prepara el 1er tile del nuevo path
                    else if (path.Count > 0)
                    {
                        PathmapTile nextTile = path[path.Count - 1];
                        SetNextTile(nextTile.TilePos);
                        path.RemoveAt(path.Count - 1);
                    }
                    IsDead = false;
                }
            }

            //Calcula la distancia y consigue una direccion
            base.OnUpdate(CurrentSpeed());
        }
        else
        {
            //Calcula el tiempo para salir de la caja
            startTimer -= Time.deltaTime;
            if(startTimer <= 0)
            {
                startRoutine = true;
            }
        }
    }

    //AI de Wander Behaviour.
    private void BehaveWander()
    {
        path.Clear();
        Vector2Int pos = mapCache.GetPosInMap(wanderTiles[wanderIndex % wanderTiles.Count].pos.position);
        PathmapTile wanderTile = mapCache.GetTile(pos.x, pos.y);
        path = mapCache.GetPath(currentTileX, currentTileY, currentDir, wanderTile.TilePos.x, wanderTile.TilePos.y, wanderTiles[wanderIndex % wanderTiles.Count].dir);
        wanderIndex++;
    }

    //Crea un path apenas comienza a seguir a pacman.
    private void BehaveChase()
    {
        MovementDirection nextDir;
        path.Clear();
        switch (ghostColor)
        {
            case GhostColor.Pink:
                nextDir = gameCache.Avatar.getCurrentDir();
                path = mapCache.GetPath(currentTileX, currentTileY, currentDir, gameCache.Avatar.GetCurrentTileX(), gameCache.Avatar.GetCurrentTileY(), nextDir);
                break;
            case GhostColor.Red:
                nextDir = gameCache.Avatar.getCurrentDir();
                if(nextDir == MovementDirection.Down)
                {
                    nextDir = MovementDirection.Up;
                }
                else if (nextDir == MovementDirection.Up)
                {
                    nextDir = MovementDirection.Down;
                }
                else if (nextDir == MovementDirection.Left)
                {
                    nextDir = MovementDirection.Right;
                }
                else if (nextDir == MovementDirection.Right)
                {
                    nextDir = MovementDirection.Left;
                }
                path = mapCache.GetPath(currentTileX, currentTileY, currentDir, gameCache.Avatar.GetCurrentTileX(), gameCache.Avatar.GetCurrentTileY(), nextDir);
                break;
            case GhostColor.Cyan:
                List<Ghost> ghosts = gameCache.GetGhostsList();
                Ghost closestGhost = null;
                for (int i = 0; i < ghosts.Count; i++)
                {
                    if (ghosts[i].ghostColor == GhostColor.Red)
                    {
                        closestGhost = ghosts[i];
                        i = ghosts.Count;
                    }
                }
                int targetX2 = gameCache.Avatar.GetCurrentTileX() + (gameCache.Avatar.GetCurrentTileX() - closestGhost.GetCurrentTileX());
                int targetY2 = gameCache.Avatar.GetCurrentTileY() + (gameCache.Avatar.GetCurrentTileY() - closestGhost.GetCurrentTileY());
                Vector2Int finalPos = mapCache.GetClosestToPos(targetX2, targetY2);
                path = mapCache.GetPath(currentTileX, currentTileY, currentDir, finalPos.x, finalPos.y, MovementDirection.None);
                break;
            case GhostColor.Orange:
                System.Random rng = new System.Random();
                int targetX = gameCache.Avatar.GetCurrentTileX() + rng.Next(-orangeRandomRadius, orangeRandomRadius);
                int targetY = gameCache.Avatar.GetCurrentTileY() + rng.Next(-orangeRandomRadius, orangeRandomRadius);
                Vector2Int finalPos2 = mapCache.GetClosestToPos(targetX, targetY);
                path = mapCache.GetPath(currentTileX, currentTileY, currentDir, finalPos2.x, finalPos2.y, MovementDirection.None);
                break;
        }
        
    }
    
    //Cuando es vulnerable invierte la direccion y apunta a una posicion random
    private void BehaveVulnerable()
    {
        System.Random rng = new System.Random();
        int targetX = gameCache.Avatar.GetCurrentTileX() + rng.Next(0, mapCache.mapSize.x-1);
        int targetY = gameCache.Avatar.GetCurrentTileY() + rng.Next(0, mapCache.mapSize.y - 1);
        Vector2Int finalPos2 = mapCache.GetClosestToPos(targetX, targetY);
        path = mapCache.GetPath(currentTileX, currentTileY, currentDir, finalPos2.x, finalPos2.y, MovementDirection.None);
    }

    //Cuando muere cambia el sprite y se mueve a la base de los fantasmas.
    public void Die()
    {
        path.Clear();
        isClaimable = false;
        SetSprite(deadSprite);
        path = mapCache.GetPath(currentTileX, currentTileY, MovementDirection.None, basePos.x, basePos.y, MovementDirection.None);
    }

    //Cuando mata a Pacman, reinicia sus stats y reinicia su tiempo de salida.
    public void Kill()
    {
        path.Clear();
        isClaimable = false;
        IsDead = false;
        isMoving = true;
        nextTileX = currentTileX;
        nextTileY = currentTileY;
        if (startRoutine || startTimer >= respawnTime*3)
        {
            if (startRoutine)
                justKilled = true;
            startRoutine = false;
            startTimer = respawnTime;
            switch (ghostColor)
            {
                case GhostColor.Red:
                    startTimer *= 0;
                    break;
                case GhostColor.Pink:
                    startTimer *= 1;
                    break;
                case GhostColor.Cyan:
                    startTimer *= 2;
                    break;
                case GhostColor.Orange:
                    startTimer *= 3;
                    break;
            }
        }
    }

    //Calcula su velocidad en base a su ambiente.
    private float CurrentSpeed()
    {
        float speed = gameCache.GetSpeed();
        if (mapCache.GetTile(currentTileX, currentTileY).PortalArea)
        {
            speed *= currentSpeed.tunnelSpeed;
        }
        else if (isClaimable)
        {
            speed *= currentSpeed.frightSpeed;
        }
        else if(IsDead)
        {
            speed *= (currentSpeed.normalSpeed*2);
        }
        else
        {
            speed *= currentSpeed.normalSpeed;
        }
        return speed;
    }

    //Detecta si puede salir de la caja
    public void CheckLeaveCage(int dotCount)
    {
        if (!startRoutine && !justKilled)
        {
            if (leaveCage.minDots <= dotCount)
            {
                startRoutine = true;
            }
            else
            {
                startTimer = leaveCage.minTime;
            }
        }
    }
}
