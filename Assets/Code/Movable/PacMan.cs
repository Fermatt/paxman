﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacMan : MobileEntity
{
    //Struct que maneja las velocidades de pacman
    [Serializable]
    public struct SpeedInLevel
    {
        public int toLevel;

        [Range(0f, 1f)]
        public float normalSpeed;

        [Range(0f, 1f)]
        public float normalDotSpeed;

        [Range(0f, 1f)]
        public float frightSpeed;

        [Range(0f, 1f)]
        public float frightDotSpeed;
    }
    
    [SerializeField] private List<SpeedInLevel> speedInLevel;

    private SpeedInLevel currentSpeed;

    private Vector2Int nextMovement;
    private Vector2Int currentMovement;
    private float dotTimer = 0;
    private float frightTimer = 0;
    private Animator anim;
    private string nextAnim = null;
    private ParticleSystem ps;

    private bool isDead;
    public bool IsDead
    {
        get
        {
            return isDead;
        }
        set
        {
            isDead = value;
        }
    }

    private void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        anim.speed = 0;
        ps = GetComponentInChildren<ParticleSystem>();
        //Toma las velocidades en base al nivel actual
        for(int i = 0; i < speedInLevel.Count; i++)
        {
            if(gameCache.GetLevelCounter() <= speedInLevel[i].toLevel || speedInLevel[i].toLevel == -1)
            {
                currentSpeed = speedInLevel[i];
                i = speedInLevel.Count;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            MovementInput();
            //Movimiento de Pacman
            MoveAvatar();

            base.OnUpdate(CurrentSpeed());
        }
    }

    //Movimiento de PacMan
    private void MoveAvatar()
    {
        //Indica el proximo tile en base al input anterior.
        int nextTileX = GetCurrentTileX() + nextMovement.x;
        int nextTileY = GetCurrentTileY() - nextMovement.y;

        int prevNextTileX = GetCurrentTileX() + currentMovement.x;
        int prevNextTileY = GetCurrentTileY() - currentMovement.y;

        //Detecta si esta en el tile destino y si se puede mover al siguiente
        if (IsAtDestination())
        {
            if (mapCache.TileIsValid(nextTileX, nextTileY) && !mapCache.TileIsStart(nextTileX, nextTileY) && !(nextTileX == GetCurrentTileX() && nextTileY == currentTileY))
            {
                currentMovement = nextMovement;
                if (nextAnim != null)
                {
                    anim.Play(nextAnim);
                    anim.speed = 1;
                }
                SetNextTile(new Vector2Int(nextTileX, nextTileY));
            }
            else if(mapCache.TileIsValid(prevNextTileX, prevNextTileY) && !mapCache.TileIsStart(prevNextTileX, prevNextTileY))
            {
                SetNextTile(new Vector2Int(prevNextTileX, prevNextTileY));
            }
            else
            {
                anim.speed = 0;
            }
        }
        nextMovement = Vector2Int.zero;
    }

    //Detecta el input, settea la direccion de movimiento y animacion.
    public void MovementInput()
    {
        if (Input.GetAxis("Vertical") > 0)
        {
            nextMovement = new Vector2Int(0, 1);
            nextAnim = "PacmanUp";
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            nextMovement = new Vector2Int(0, -1);
            nextAnim = "PacmanDown";
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            nextMovement = new Vector2Int(1, 0);
            nextAnim = "PacmanRight";
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            nextMovement = new Vector2Int(-1, 0);
            nextAnim = "PacmanLeft";
        }
    }

    //Cambia la velocidad inicial en base al ambiente.
    private float CurrentSpeed()
    {
        dotTimer -= Time.deltaTime;
        frightTimer -= Time.deltaTime;
        float speed = gameCache.GetSpeed();
        if(frightTimer>0)
        {
            if (dotTimer > 0)
            {
                speed *= currentSpeed.frightDotSpeed;
            }
            else
            {
                speed *= currentSpeed.frightSpeed;
            }
        }
        else if(dotTimer > 0)
        {
            speed *= currentSpeed.normalDotSpeed;
        }
        else
        {
            speed *= currentSpeed.normalSpeed;
        }
        return speed;
    }

    public MovementDirection getCurrentDir()
    {
        return currentDir;
    }

    //Settea el timer de velocidad para cuando Pacman toma un punto.
    public void SetDotTimer(float t)
    {
        dotTimer = t;
    }
    //Settea el timer de velocidad para cuando los fantasmas son vulnerables
    public void SetFrightTimer(float t)
    {
        frightTimer = t;
    }

    //Activa las particulas de muerte.
    public void PlayParticleSystem()
    {
        if(ps)
            ps.Play();
    }

    //Animacion de muerte
    public void PlayDeadAnim()
    {
        isMoving = false;
        anim.speed = 1;
        anim.Play("PacmanDead");
    }

    //Direccion inicial
    public void PlayStartAnim()
    {
        anim.speed = 1;
        anim.Play("PacmanRight");
        nextMovement = Vector2Int.zero;
        currentMovement = Vector2Int.zero;
    }

    public void StopAnim()
    {
        anim.speed = 0;
    }
}