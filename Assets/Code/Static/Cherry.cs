﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cherry : StaticEntity
{
    //Guarda la imagen, el valor y el nivel en el que se encuentra.
    [Serializable]
    private struct Fruit
    {
        public Sprite sprite;
        public uint score;
        public int level;
    }
    
    [SerializeField] private Fruit[] fruitType;

    [SerializeField] private float time = 9.3f;

    private float timer = 0;

    //Actualiza el sprite y el puntaje
    private void Start()
    {
        base.Start();
        for(int i = 0; i < fruitType.Length; i++)
        {
            if(gameCache.GetLevelCounter() <= fruitType[i].level || fruitType[i].level == -1)
            {
                score = fruitType[i].score;
                GetComponent<SpriteRenderer>().sprite = fruitType[i].sprite;
                i = fruitType.Length;
            }
        }
    }

    //Destruye el objeto cuando colisiona con pacman
    void Update()
    {
        if (HasIntersectedPacman())
        {
            Destroy(this.gameObject);
        }
        timer += Time.deltaTime;
        if(timer > time)
        {
            Destroy(this.gameObject);
        }
    }
}
