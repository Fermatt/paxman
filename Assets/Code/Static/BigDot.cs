﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigDot : StaticEntity
{
    void Update()
    {
        //Destruye el objeto cuando colisiona con pacman
        if (HasIntersectedPacman())
        {
            Destroy(this.gameObject);
            mapCache.DotCount--;

            gameCache.BigDotStart();
        }
    }
}
