﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticEntity : Entity
{
    [SerializeField] protected uint score = 10;
    [SerializeField] private float collisionRadius = 5f;

    private void Start()
    {
        base.Start();
    }

    //Detecta si colisiona con un punto
    protected bool HasIntersectedPacman()
    {
        if (gameCache.Avatar != null)
        {
            if ((this.GetPosition() - gameCache.Avatar.GetPosition()).magnitude < collisionRadius)
            {
                gameCache.UpdateScore(score);
                return true;
            }
        }
        return false;
    }
}
