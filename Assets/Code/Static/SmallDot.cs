﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallDot : StaticEntity
{
    [SerializeField] private float speedTimer;

    void Update()
    {
        //Destruye el objeto cuando colisiona con pacman y actualiza sus dependientes
        if (HasIntersectedPacman())
        {
            Destroy(this.gameObject);
            mapCache.DotCount--;
            List<Ghost> g = gameCache.GetGhostsList();
            for (int i = 0; i < g.Count; i++)
            {
                g[i].CheckLeaveCage(mapCache.maxDotCount - mapCache.DotCount);
            }
            gameCache.Avatar.SetDotTimer(speedTimer);
        }
    }
}
