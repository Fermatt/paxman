﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    protected Game gameCache;
    protected Map mapCache;

    //Referencias a map y a game
    protected void Start()
    {
        mapCache = Map.instance;
        gameCache = Game.instance;
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }

    public void SetPosition(Vector2 position)
    {
        transform.position = position;
    }
}
